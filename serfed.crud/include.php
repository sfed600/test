<?php
Bitrix\Main\Loader::registerAutoloadClasses(
	"serfed.crud",
	array(
		"Serfed\\Test" => "lib/test.php",
		"Serfed\\DataTable" => "lib/data.php",
	)
);
