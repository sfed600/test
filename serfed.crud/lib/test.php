<?php

namespace Serfed;

use \Bitrix\Main\Web\Json;

class Test{

    public static function get() {
        $result = DataTable::getList(
                        array(
                            'select' => array('*')
        ));

        $row = $result->fetch();

        echo Json::encode($row);
    }

    public static function delete($primary) {
        $result = DataTable::delete(
            $primary
        );
    }

    public static function update($primary, $data) {
        $result = DataTable::update(
            $primary,
            $data
        );
    }

    public static function add($data) {
        $result = DataTable::add(
            $data
        );
    }
}
