<?php
namespace Serfed;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
 

/**
 * Class DataTable
 * 
 * Fields:
 * <ul>
 * <li> id int mandatory
 * <li> name string mandatory
 * <li> address string mandatory
 * <li> UPDATED datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> CREATED datetime mandatory default 'CURRENT_TIMESTAMP'
 * </ul>
 *
  **/
 
class DataTable extends Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'serfed_test';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'id' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('DATA_ENTITY_ID_FIELD'),
			),
			'name' => array(
				'data_type' => 'text',
				'required' => true,
				'title' => Loc::getMessage('DATA_ENTITY_TITLE_FIELD'),
			),
			'address' => array(
				'data_type' => 'text',
				'required' => true,
				'title' => Loc::getMessage('DATA_ENTITY_ADDRESS_FIELD'),
			),
			'updated_at' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('DATA_ENTITY_UPDATED_FIELD'),
			),
			'created_at' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('DATA_ENTITY_CREATED_FIELD'),
			),
		);
	}
}